/**
 * Created by malcolmross on 12/6/17.
 */

import React, { Component } from 'react'
import ExpenseList from '../components/ExpenseList'


class Expense extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expenses: [],
            expenseNum: 0,
            expenseName: '',
            expenseAmount: '',
            message: ''
        }

        this.inputChangeName = this.inputChangeName.bind(this);
        this.inputChangeAmount = this.inputChangeAmount.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    inputChangeName(e) {
        e.preventDefault()
        this.setState({expenseName: e.target.value})
    }

    inputChangeAmount(e) {
        e.preventDefault()
        this.setState({expenseAmount: e.target.value})
    }

    handleSubmit(e) {
        e.preventDefault()
        const expenseName = this.expenseName.value
        const expenseAmount = this.expenseAmount.value
        let errors = this.validate(expenseName, expenseAmount)

        if(errors.length > 0){
            this.setState({
                message: errors
            })
        } else {
            this.setState({expenseNum: this.state.expenseNum + 1})
            expenseName !== '' && expenseAmount !== '' && this.state.expenses.push({'expenseNum':this.state.expenseNum + 1,
                'expense':this.state.expenseName, 'amount':this.state.expenseAmount})
            this.setState({expenses: this.state.expenses, expenseName: "", expenseAmount: "", message: ''})
        }
    }

    removeExpense(expenseToRemove){
        const newExpensesList = this.state.expenses.filter(expense => {
            return expense !== expenseToRemove
        })

        this.setState({
            expenses: [...newExpensesList]
        })
    }

    validate(expense, amount){
        let errors = []
        if(expense.length === 0){
            errors.push('The name field can not be empty!')
            this.expenseName.style.borderColor = 'red'
        }
        if(amount.length === 0){
            errors.push('The amount field can not be empty!')
            this.expenseAmount.style.borderColor = 'red'
        }
        if(isNaN(amount)){
            errors.push('The amount must be a number!')
            this.expenseAmount.style.borderColor = 'red'
        } else {
            this.expenseName.style.borderColor = ''
            this.expenseAmount.style.borderColor = ''
            errors = []
        }
        return errors
    }




    render() {
        let myExpenses = this.state.expenses.map((val, key) => {
            return <ExpenseList key={key} keyval={key} val={val} delMe={() => this.removeExpense(key) }/>
        })

        return (
            <section className="App">
                <header>
                    <h1>Expenses</h1>
                </header>
                <section className="row">
                    <h2>Add Expense</h2>
                    {
                        this.state.message !== '' && <p className="message text-danger">{this.state.message}</p>
                    }
                    <form className="form-inline" onSubmit={(e) => {this.handleSubmit(e)}}>
                        <div className="form-group">
                            <label htmlFor="eName">Name</label>
                            <input ref={(input) => {this.expenseName = input}} type="text" name="name" id="eName" value={this.state.expenseName} onChange={this.inputChangeName} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="eAmount">Amount</label>
                            <input ref={(input) => {this.expenseAmount = input}} type="number" name="amount" id="eAmount" value={this.state.expenseAmount} onChange={this.inputChangeAmount} />
                        </div>

                        <button type="submit" className="btn btn-primary">Add</button>
                    </form>

                </section>
                <section className="Expenses">
                    <h2>Current Expenses</h2>
                    <ul>{myExpenses}</ul>
                </section>
            </section>
        )
    }
}


export default Expense

