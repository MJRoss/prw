
import React, { Component } from 'react'
import FaTrash from 'react-icons/lib/fa/trash'

class ExpenseList extends Component {

    removeExpense(key){
        this.props.removeExpense(key)
    }

    render() {
        return (
            <li key={this.props.keyval}>
                <span>{this.props.val.expenseNum}</span>
                <span>{ this.props.val.expense }</span>
                <span>${ this.props.val.amount }</span>
                <span><button className="btn btn-small" onClick={this.props.removeExpense}><FaTrash /></button></span>
            </li>
        )
    }
}

export default ExpenseList